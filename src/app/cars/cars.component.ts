import { Component, OnInit, Input, Output, EventEmitter} from '@angular/core';
import { Car} from "./shared/Car";
import { CarDetailComponent} from "./car-detail/car-detail.component";
import { CarListComponent} from "./car-list/car-list.component";
import { CarService} from "./car.service";

@Component({
  selector: 'app-cars',
  templateUrl: './cars.component.html',
  styleUrls: ['./cars.component.css']
})
export class CarsComponent implements OnInit {


  cars = [];
  selectedCar : Car;

  constructor(private carService: CarService) {
  }

  ngOnInit() {
  this.carService.GetAllCars().subscribe(cars =>this.cars = cars);
    }

  onSelect(car: Car): void {
    this.selectedCar = car;
  }

  /*
  onSelect(args){
  let Car=this.cars.findIndex((x=>x===this.cars[args.index].id))
  console.log(Car);
  };*/
}

