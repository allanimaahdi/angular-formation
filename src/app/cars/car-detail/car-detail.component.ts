import {Component, Input, OnInit} from '@angular/core';
import {Car} from "../shared/Car";
import {CarService} from "../car.service";

@Component({
  selector: 'app-car-detail',
  templateUrl: './car-detail.component.html',
  styleUrls: ['./car-detail.component.css']
})
export class CarDetailComponent implements OnInit {
  @Input() car: Car;
  constructor(private carService: CarService) { }

  ngOnInit() {
   this.carService.GetCar(this.car.id).subscribe(car=> this.car = car);
  }

}
