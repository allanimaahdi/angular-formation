import { Component, OnInit } from '@angular/core';
import { CarService } from "../car.service";
import { Car } from "../shared/Car";

@Component({
  selector: 'app-car-create',
  templateUrl: './car-create.component.html',
  styleUrls: ['./car-create.component.css']
})
export class CarCreateComponent implements OnInit {
  newCar: Car;
  constructor(private _carService:CarService) { }
  model = new Car(0, "", "",0,0,0,0);

  ngOnInit() {
  }
  addNewCar(){
    this.newCar = new Car(0, "", "",0,0,0,0);
  }
  onSubmit(){
    console.log("submit new car");
    this._carService.PostCar(this.newCar).subscribe(res=> {
      console.log(res);
      location.reload();
    })
  }
}
