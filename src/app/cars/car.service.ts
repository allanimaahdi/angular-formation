import { Inject, Injectable } from '@angular/core';
import { Car} from "./shared/Car";
import { of } from 'rxjs'
import { Observable } from "rxjs";
import { APPCONFIG } from '../app.config'
import { HttpClient } from "@angular/common/http";
import {map} from 'rxjs/operators'
@Injectable()
export class CarService {

  constructor(private http: HttpClient, @Inject(APPCONFIG.CARS_API_URL) private apiUrl) {

  }

  GetAllCars(): Observable<Car[]>{
    return this.http.get<Car[]>(this.apiUrl ).pipe(map((res:any)=> res.result));

  };
  GetCar(id: number): Observable<Car>{
    return this.http.get<Car>(this.apiUrl + {id})
  }
  PostCar(car: Car){
    return this.http.post(this.apiUrl+car.id, car)
  };
  PutCar(car: Car){
    return this.http.put(this.apiUrl+car.id, car);
  };
  DeleteCar(car: Car){};
}
