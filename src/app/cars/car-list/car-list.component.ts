import { Component, OnInit, EventEmitter, Input, Output } from '@angular/core';
import {Car} from "../shared/Car";

@Component({
  selector: 'app-car-list',
  templateUrl: './car-list.component.html',
  styleUrls: ['./car-list.component.css']
})
export class CarListComponent implements OnInit {
  @Input() cars: Car[];
  @Output() onSelected = new EventEmitter<Car>();
  constructor() { }

  ngOnInit() {
  }
  onSelect(selectedCar: Car): void{
    this.onSelected.emit(selectedCar)
  }

}
