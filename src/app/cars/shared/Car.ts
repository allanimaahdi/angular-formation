import { Vehicle} from "./Vehicle";

export class Car implements Vehicle {
    private _isRunning: boolean;
    constructor (public id: number,
                 public brand: string,
                 public model: string,
                 public horsePower: number,
                 public weight: number,
                 public price: number,
                 public nbSeats: number){

        this._isRunning= false
    }

    start(): boolean{
        if(this._isRunning){
            console.log("this cas is already started");
            return false;
        }
        else {
            console.log("car is starting...");
            this._isRunning = true;
            return true;
        }
    }
    stop (): boolean{
        if (!this._isRunning){
            console.log("this car is already stopped");
            return false;
        }
        else{
            console.log("car stopping...");
            this._isRunning = false;
            return true;
        }
    }

    move(d: number):void {
        if (this._isRunning){
            console.log("this car moved " + d + "meters.");
        }
        else {
            console.log("this car isn't started !");
        }
    }


}