export interface Vehicle {
    // Fields
    id: number;
    weight: number;
    price: number;
    nbSeats: number;
// Methods
    start(): boolean;
    stop(): boolean;
    move(d: number): void;
}
