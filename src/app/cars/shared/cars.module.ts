import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CarsComponent } from "../cars.component";
import { FormsModule } from "@angular/forms";
import { CarDetailComponent} from "../car-detail/car-detail.component";
import { CarListComponent} from "../car-list/car-list.component";
import {APPCONFIG} from "../../app.config";
import {HttpClientModule} from "@angular/common/http";
import {CarService} from "../car.service";
import {CarCreateComponent} from "../car-create/car-create.component";

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    HttpClientModule
  ],
  declarations: [CarsComponent,CarListComponent,CarDetailComponent,CarCreateComponent],
  providers:[CarService],
  exports: [CarsComponent, CarDetailComponent, CarListComponent,CarCreateComponent]
})
export class CarsModule { }
