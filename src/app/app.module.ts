import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import {CarsModule} from "./cars/shared/cars.module";
import {CarService} from "./cars/car.service";
import {APPCONFIG} from "./app.config";
import {HttpClient, HttpClientModule} from "@angular/common/http";
import { CarCreateComponent } from './cars/car-create/car-create.component';
import {FormsModule} from "@angular/forms";

@NgModule({
  declarations: [
    AppComponent,


  ],
  imports: [
    BrowserModule,
    CarsModule,
    HttpClientModule,
    FormsModule,

  ],
  providers:[{provide: APPCONFIG.CARS_API_URL, useValue: APPCONFIG.CARS_API_URL}],
  bootstrap: [AppComponent]
})
export class AppModule { }
